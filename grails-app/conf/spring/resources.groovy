import source.corp.task.RequestStatus

// Place your Spring DSL code here
beans = {

    httpClient(wslite.http.HTTPClient) {
        connectTimeout = 5000
        readTimeout = 10000
        useCaches = false
        followRedirects = false
        sslTrustAllCerts = true
    }

    weatherSoapClient(wslite.soap.SOAPClient) {
        serviceURL = "https://graphical.weather.gov:443/xml/SOAP_server/ndfdXMLserver.php"
        httpClient = ref('httpClient')
    }
}
