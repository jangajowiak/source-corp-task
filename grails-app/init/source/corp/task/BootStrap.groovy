package source.corp.task

import grails.converters.JSON

import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

class BootStrap {

    def init = { servletContext ->
        ExecutorServiceWrapper.init()

        JSON.registerObjectMarshaller(RequestStatus) { RequestStatus requestStatus ->
            return requestStatus.name()
        }
        JSON.registerObjectMarshaller(RequestType) { RequestType requestType ->
            return requestType.name()
        }
        JSON.registerObjectMarshaller(LocalDateTime) { LocalDateTime localDateTime ->
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
            return localDateTime.format(formatter)
        }
    }
    def destroy = {
        ExecutorServiceWrapper.destroy()
    }
}
