package source.corp.task

class UrlMappings {

    static mappings = {
        "/$controller/$action?/$id?(.$format)?" {
            constraints {
                // apply constraints here
            }
        }

        "/"(view: "/index")
        "500"(view: '/error')
        "404"(view: '/notFound')

        group "/weather", {
            "/"(controller: "weatherRequest") { action = [GET: "getRequests", POST: "addRequest"] }
            "/requests.xls"(controller: "weatherRequest") { action = [GET: "exportRequests", POST: "exportRequests"] }
            "/$id"(controller: "weatherRequest") { action = [GET: "getRequestById", DELETE: "cancelRequest"] }
            "/$id/response"(controller: "weatherRequest") { action = [GET: "getRequestResponse"] }
        }
    }
}
