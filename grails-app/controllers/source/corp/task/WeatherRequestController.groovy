package source.corp.task

import grails.converters.JSON

class WeatherRequestController {

    WeatherRequestService weatherRequestService
    WeatherRequestExcelService weatherRequestExcelService

    def getRequests() {
        render weatherRequestService.getRequests() as JSON
    }

    def exportRequests() {
        render file: weatherRequestExcelService.exportRequests(), contentType: 'application/vnd.ms-excel'
    }

    def getRequestById(Long id) {
        render weatherRequestService.getRequest(id) as JSON
    }

    def cancelRequest(Long id) {
        render weatherRequestService.cancelRequest(id) as JSON
    }

    def getRequestResponse(Long id) {
        render weatherRequestService.getRequestResponse(id)
    }

    def addRequest() {
        render weatherRequestService.addRequest(params.query, params.type as RequestType) as JSON
    }
}
