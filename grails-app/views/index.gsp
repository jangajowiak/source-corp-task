<!doctype html>
<%@ tagliburi="isomorphic" prefix="isomorphic" %>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>Title</title>

    <script>varisomorphicDir = "../isomorphic2saf/";</script>
    <asset:javascript src="system/modules/ISC_Core.js"/>
    <asset:javascript src="system/modules/ISC_Foundation.js"/>
    <asset:javascript src="system/modules/ISC_Containers.js"/>
    <asset:javascript src="system/modules/ISC_Grids.js"/>
    <asset:javascript src="system/modules/ISC_Forms.js"/>
    <asset:javascript src="system/modules/ISC_DataBinding.js"/>
    <asset:javascript src="/assets/skins/SmartClient/load_skin.js"/>

</head>

<body>
%{--<div>--}%
    %{--<h4>Search for coordinates based on zip code:</h4>--}%
    %{--<g:form controller="weatherRequestView">--}%
        %{--<g:field type="hidden" name="type" required="" value="ZIP_CODE"/>--}%
        %{--<g:field type="text" name="query" required="" placeholder="here enter zip code"/>--}%
        %{--<button type="submit">Queue search</button>--}%
    %{--</g:form>--}%

    %{--<h4>Get area of sector</h4>--}%
    %{--<g:form controller="weatherRequestView">--}%
        %{--<g:field type="hidden" name="type" required="" value="CORNERS"/>--}%
        %{--<g:field type="text" name="query" required="" placeholder="here enter sector"/>--}%
        %{--<button type="submit">Queue search</button>--}%
    %{--</g:form>--}%
%{--</div>--}%

<div>
    <script>

        isc.DataSource.create({
            ID:"requestsSource",
            dataFormat:"json",
            dataURL:"/weather",
            fields:[
                {title:"Request data&time", name:"dateCreated"},
                {title:"Request type", name:"type"},
                {title:"User's input", name:"query"},
                {title:"Status", name:"status"},
                {title:"Start date&time", name:"startTime"},
                {title:"Completion date&time", name:"completionTime"},
                {title:"Fetch result", name:"resultButton"}
            ]
        });



        isc.VLayout.create({
            ID: "vLayoutForms",
            width: "100%",
            height: "100%",
            members: [
                isc.HStack.create({
                    width: "100%",
                    members: [
                        isc.DynamicForm.create({
                            ID: "zipForm",
                            width: 220,
                            fields: [
                                {
                                    height: 40,
                                    name: "zip",
                                    title: "zip",
                                    type: "text",
                                    required: true,
                                    defaultValue: ""
                                }
                            ]
                        }),
                        isc.Button.create({
                            width: 250,
                            title: "Queue search",
                            click : function () {
                                var refreshData = function() {
                                    resourceList.invalidateCache()
                                };
                                isc.DataSource.create({
                                    ID:"requestsZip",
                                    dataFormat:"json",
                                    dataURL:"/weather?type=ZIP_CODE&query=" + zipForm.values.zip
                                }).fetchData(null, refreshData, {httpMethod: 'POST'});
                            }
                        })
                    ],
                    membersMargin: 5
                }),
                isc.HStack.create({
                    width: "100%",
                    members: [
                        isc.DynamicForm.create({
                            ID: "sectorForm",
                            width: 250,
                            fields: [
                                {
                                    height: 40,
                                    name: "sector",
                                    title: "sector",
                                    type: "text",
                                    required: true,
                                    defaultValue: ""
                                }
                            ]
                        }),
                        isc.Button.create({
                            width: 220,
                            title: "Queue search",
                            click : function () {
                                var refreshData = function() {
                                    resourceList.invalidateCache()
                                };
                                isc.DataSource.create({
                                    ID:"requestsZip",
                                    dataFormat:"json",
                                    dataURL:"/weather?type=CORNERS&query=" + zipForm.values.sector
                                }).fetchData(null, refreshData, {httpMethod: 'POST'});
                            }
                        })
                    ],
                    membersMargin: 5
                }),
                isc.ListGrid.create({
                    ID: "resourceList",
                    width: "100%",
                    height: "100%",
                    top: 50,
                    dataSource: "requestsSource",
                    autoFetchData: true,
                    showRecordComponents: true,
                    showRecordComponentsByCell: true,
                    createRecordComponent : function (record, colNum) {
                        var fieldName = this.getFieldName(colNum);
                        if (fieldName == "resultButton" && record["result"]) {
                            var button = isc.IButton.create({
                                title: "Get",
                                height: 26,
                                width: 65,
                                layoutAlign: "center",
                                click : function () {
                                    isc.say(record["result"]);
                                }
                            });
                            return button;
                        }
                        return null;
                    }
                }),
                isc.Button.create({
                    title: "Excel",
                    left: 60,
                    click : function () {
                        var dsRequest = {
                            operationId: "downloadDescriptions",
                            downloadResult: true
                        };
                        isc.DataSource.create({
                            ID:"requestsXls",
                            dataURL:"/weather/requests.xls"
                        }).fetchData(null, null, dsRequest);
                    }
                })
            ]
        });

    </script>

</div>

</body>
</html>
