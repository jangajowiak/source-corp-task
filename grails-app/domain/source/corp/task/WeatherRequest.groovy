package source.corp.task

import java.time.LocalDateTime

class WeatherRequest {

    LocalDateTime dateCreated
    RequestType type
    String query
    RequestStatus status
    LocalDateTime startTime
    LocalDateTime completionTime
    String result

    static constraints = {
        result nullable: true
        startTime nullable: true
        completionTime nullable: true
        result maxSize: 50000
    }

    static mapping = {
        autoTimestamp true
    }

    boolean isZipCodeType() {
        RequestType.ZIP_CODE == type
    }

    boolean isQueued() {
        RequestStatus.QUEUED == status
    }
}
