package source.corp.task

import grails.gorm.transactions.Transactional
import org.apache.poi.ss.usermodel.Cell
import org.apache.poi.ss.usermodel.CellStyle
import org.apache.poi.ss.usermodel.Font
import org.apache.poi.ss.usermodel.IndexedColors
import org.apache.poi.ss.usermodel.Row
import org.apache.poi.ss.usermodel.Sheet
import org.apache.poi.ss.usermodel.Workbook
import org.apache.poi.xssf.usermodel.XSSFCellStyle
import org.apache.poi.xssf.usermodel.XSSFFont
import org.apache.poi.xssf.usermodel.XSSFSheet
import org.apache.poi.xssf.usermodel.XSSFWorkbook

import java.sql.Timestamp
import java.time.LocalDateTime

@Transactional
class WeatherRequestExcelService {

    public static final String SHEET_NAME = "Requests"
    public static final String TIME_CELL_FORMAT = "yyyy-MM-dd"
    public static final short HEADER_FONT_SIZE = 12

    WeatherRequestService weatherRequestService

    private static String[] columns = ["Request date&time", "Request type", "User's input", "Status", "Start date&time", "Complation date&time"]

    def exportRequests() {
        Workbook workbook = new XSSFWorkbook()
        Sheet sheet = workbook.createSheet(SHEET_NAME)
        prepareHeader(workbook, sheet)
        prepareRows(workbook, sheet)
        autoSizeColumns(sheet)
        return createStream(workbook)
    }

    private void prepareHeader(XSSFWorkbook workbook, XSSFSheet sheet) {
        XSSFCellStyle headerCellStyle = createHeaderCellStyle(workbook)
        Row headerRow = sheet.createRow(0)
        for (int i = 0; i < columns.length; i++) {
            createCell(headerRow, i, columns[i], headerCellStyle)
        }
    }

    private void prepareRows(XSSFWorkbook workbook, XSSFSheet sheet) {
        XSSFCellStyle dateCellStyle = createTimeCellStyle(workbook)
        def weatherRequests = weatherRequestService.getRequests()
        for (int i = 0; i < weatherRequests.size(); i++) {
            WeatherRequest weatherRequest = weatherRequests.get(i)
            Row row = sheet.createRow(i+1)
            createDateCell(row, 0, weatherRequest.dateCreated, dateCellStyle)
            createCell(row, 1, weatherRequest.type.name())
            createCell(row, 2, weatherRequest.query)
            createCell(row, 3, weatherRequest.status.name())
            if(weatherRequest.startTime) {
                createDateCell(row, 4, weatherRequest.startTime, dateCellStyle)
            }
            if(weatherRequest.completionTime) {
                createDateCell(row, 5, weatherRequest.completionTime, dateCellStyle)
            }
        }
    }

    private createDateCell(row, index, LocalDateTime time, style = null) {
        createCell(row, index, Timestamp.valueOf(time), style)
    }

    private createCell(row, index, value, style = null) {
        Cell cell = row.createCell(index)
        cell.setCellValue(value)
        if (style) {
            cell.setCellStyle(style)
        }
    }

    private ByteArrayInputStream createStream(XSSFWorkbook workbook) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream()
        workbook.write(stream)
        workbook.close()
        return new ByteArrayInputStream(stream.toByteArray())
    }

    private void autoSizeColumns(XSSFSheet sheet) {
        for (int i = 0; i < columns.length; i++) {
            sheet.autoSizeColumn(i);
        }
    }

    private XSSFCellStyle createTimeCellStyle(XSSFWorkbook workbook) {
        CellStyle dateCellStyle = workbook.createCellStyle()
        dateCellStyle.setDataFormat(workbook.getCreationHelper().createDataFormat().getFormat(TIME_CELL_FORMAT))
        dateCellStyle
    }

    private XSSFCellStyle createHeaderCellStyle(XSSFWorkbook workbook) {
        CellStyle headerCellStyle = workbook.createCellStyle();
        headerCellStyle.setFont(getHeaderFont(workbook));
        headerCellStyle
    }

    private XSSFFont getHeaderFont(XSSFWorkbook workbook) {
        Font headerFont = workbook.createFont();
        headerFont.setBold(true);
        headerFont.setFontHeightInPoints(HEADER_FONT_SIZE);
        headerFont.setColor(IndexedColors.GREEN.getIndex());
        headerFont
    }
}
