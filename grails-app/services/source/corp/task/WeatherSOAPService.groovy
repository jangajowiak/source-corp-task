package source.corp.task

import grails.gorm.transactions.Transactional

import java.time.LocalDateTime

@Transactional
class WeatherSOAPService {

    public static final String ACTION_NAME = "https://graphical.weather.gov/xml/DWMLgen/wsdl/ndfdXML.wsdl#"

    def weatherSoapClient

    @Transactional
    processRequest(Long weatherRequestId) {
        WeatherRequest weatherRequest = WeatherRequest.findById(weatherRequestId)
        if(weatherRequest.isQueued()) {
            executeRequest(weatherRequest)
        }
    }

    private def executeRequest(WeatherRequest weatherRequest) {
        beforeProcessing(weatherRequest)
        try {
            def result = fetchWeatherData(weatherRequest)
            afterProcessing(weatherRequest, result.text)
        } catch (Exception e) {
            errorProcessing(weatherRequest)
        }
    }

    private def fetchWeatherData(WeatherRequest weatherRequest) {
        return weatherRequest.isZipCodeType() ? getLatLonListZipCode(weatherRequest.query) : getCornerPoints(weatherRequest.query)
    }

    private void afterProcessing(WeatherRequest weatherRequest, String result) {
        weatherRequest.result = result
        weatherRequest.completionTime = LocalDateTime.now()
        setStatusAndSave(weatherRequest, RequestStatus.SUCCESS)
    }

    private void beforeProcessing(WeatherRequest weatherRequest) {
        weatherRequest.startTime = LocalDateTime.now()
        setStatusAndSave(weatherRequest, RequestStatus.IN_PROGRESS)
    }

    private void errorProcessing(WeatherRequest weatherRequest) {
        setStatusAndSave(weatherRequest, RequestStatus.FAILED)
    }

    private void setStatusAndSave(WeatherRequest weatherRequest, RequestStatus status) {
        weatherRequest.status = status
        weatherRequest.save()
    }

    private getLatLonListZipCode(String zipCode) {
        String action = ACTION_NAME + "LatLonListZipCode"
        weatherSoapClient.send(SOAPAction: action) {
            body {
                LatLonListZipCodeRequest() {
                    zipCodeList(zipCode)
                }
            }
        }
    }

    private getCornerPoints(String zipCode) {
        String action = ACTION_NAME + "CornerPoints"
        weatherSoapClient.send(SOAPAction: action) {
            body {
                CornerPointsRequest() {
                    sector(zipCode)
                }
            }
        }
    }
}
