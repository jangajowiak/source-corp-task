package source.corp.task

import grails.gorm.transactions.Transactional

@Transactional
class WeatherRequestService {

    WeatherSOAPService weatherSOAPService

    def getRequests() {
        return WeatherRequest.listOrderByDateCreated(order: "desc")
    }

    WeatherRequest getRequest(Long id) {
        return WeatherRequest.findById(id)
    }

    WeatherRequest cancelRequest(Long id) {
        WeatherRequest weatherRequest = WeatherRequest.findById(id)
        if(weatherRequest.isQueued()) {
            weatherRequest.status = RequestStatus.CANCELLED
            weatherRequest.save()
        }
        return weatherRequest
    }

    def getRequestResponse(Long id) {
        def request = WeatherRequest.findById(id)
        return request && request.result ? request.result : ""
    }

    WeatherRequest addRequest(String query, RequestType type) {
        WeatherRequest weatherRequest = createNewRequest(query, type)
        startProcessing(weatherRequest.id)
        return weatherRequest;
    }

    private def createNewRequest(String query, RequestType type) {
        return new WeatherRequest(query: query, type: type, status: RequestStatus.QUEUED).save()
    }

    private startProcessing(Long weatherRequestId) {
        // TODO: use queues etc.
        ExecutorServiceWrapper.execute(new Runnable() {
            @Override
            void run() {
                weatherSOAPService.processRequest(weatherRequestId)
            }
        })
    }
}
