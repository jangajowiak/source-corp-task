package source.corp.task

enum RequestStatus {
    QUEUED, FAILED, SUCCESS, IN_PROGRESS, CANCELLED
}