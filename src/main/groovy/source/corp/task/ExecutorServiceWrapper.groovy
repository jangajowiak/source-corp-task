package source.corp.task

import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors

class ExecutorServiceWrapper {
    private static ExecutorService executorService

    static void init() {
        executorService = Executors.newFixedThreadPool(5);
    }

    static void destroy() {
        executorService.shutdown()
    }

    static void execute(Runnable runnable) {
        executorService.execute(runnable)
    }
}
